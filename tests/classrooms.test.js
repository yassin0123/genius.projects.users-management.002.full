const _ = require("lodash");
const { ObjectId } = require("mongodb");
const Promise = require("bluebird");
const request = require("supertest");
const random = require("randomstring");
const payload = require("./payload.json");
const { app, server } = require("../src/app");


const dbProvider = require("./services/db");

const headers = {
  Accept: "application/json",
};

const COLLECTION = {
  Classroom: "Classroom",
  User: "User",
};

const initClassroomUser = async () => {
  const classroom = _.chain(payload.classrooms)
    .head()
    .clone()
    .set("_id", ObjectId())
    .value();
  const user = _.chain(payload.users)
    .head()
    .clone()
    .set("_id", ObjectId())
    .set("classroom", classroom._id)
    .value();

  const db = await dbProvider.get();
  await db.collection(COLLECTION.Classroom).insertOne(classroom);
  await db.collection(COLLECTION.User).insertOne(user);

  return [classroom, user];
};

describe("SUCCESS", () => {
  const tokens = {
    student: undefined,
    teacher: undefined,
  };

  beforeEach(async () => {
    const db = await dbProvider.get();
    db.dropDatabase();
    await db
      .collection(COLLECTION.Classroom)
      .insertMany(_.cloneDeep(payload.classrooms));
  });

  afterEach(async () => {
    const db = await dbProvider.get();
    await db.dropDatabase();
    await dbProvider.close();
    return server.close();
  });

  // PROBE

  it("1-1 | EXPECT SUCCESS : /status to returns 200 ", async () => {
    return await request(app).get("/status").set(headers).expect(200);
  });

  // LIST

  it("2-1| EXPECT SUCCESS : /classrooms return classrooms list ", async () => {
    const db = await dbProvider.get();

    const classrooms = await db
      .collection(COLLECTION.Classroom)
      .find()
      .toArray();

    const { body } = await request(app)
      .get("/api/classrooms")
      .set(headers)
      .expect(200);

    expect(_.chain(body).value().sort()).toEqual(
      _.map(classrooms, (it) => _.set(it, "_id", it._id.toString())).sort()
    );
  });

  //GET

  it("3-1 | EXPECT FAILURE : fetch classroom with wrong id ", async () => {
    return await request(app)
      .get("/api/classrooms/:some_id")
      .set(headers)
      .expect(400);
  });

  it("3-2 | EXPECT SUCCESS : fetch classroom id ", async () => {
    const classroom = _.clone(payload.classroom);
    const db = await dbProvider.get();
    await db.collection(COLLECTION.Classroom).insertOne(classroom);

    const { body } = await request(app)
      .get(`/api/classrooms/${classroom._id}`)
      .set(headers)
      .expect(200);

    expect(body).toEqual({ ...classroom, _id: classroom._id.toString() });
  });

  // CREATE

  it("4-1 | EXPECT FAILURE : add classroom with invalid payload.classroom ", async () => {
    const invalid = {
      some_key: "wrong",
    };

    await request(app)
      .post("/api/classrooms")
      .set(headers)
      .send(invalid)
      .expect(400);

    await Promise.map(_.keys(payload.classroom), (key) =>
      request(app)
        .post("/api/classrooms")
        .set(headers)
        .send(_.omit(payload.classroom, key))
        .expect(400)
    );
  });

  it("4-2 | EXPECT SUCCESS : add classroom with correct payload.classroom ", async () => {
    const initCount = _.size(payload.classrooms);

    const { body } = await request(app)
      .post("/api/classrooms")
      .set(headers)
      .send(payload.classroom)
      .expect(201);

    const db = await dbProvider.get();
    const afterCount = await db
      .collection(COLLECTION.Classroom)
      .countDocuments();
    const classroom = await db
      .collection(COLLECTION.Classroom)
      .findOne({ _id: ObjectId(body._id) });

    expect(_.omit(body, "_id")).toEqual(payload.classroom);
    expect(afterCount).toEqual(initCount + 1);
    expect(body).toEqual({ ...classroom, _id: classroom._id.toString() });
  });

  // UPDATE

  it("5-1 | EXPECT FAILURE : update classroom with invalid id ", async () => {
    return await request(app)
      .put("/api/classrooms/:some_id")
      .send(payload.classroom)
      .set(headers)
      .expect(400);
  });

  it("5-2| EXPECT FAILURE : update classroom with invalid payload.classroom ", async () => {
    const invalid = {
      some_key: "wrong",
    };

    const _id = _.chain(payload.classrooms).head().get("_id").value();

    await request(app)
      .put(`/api/classrooms/${_id}`)
      .set(headers)
      .send(invalid)
      .expect(400);

    await Promise.map(_.keys(payload.classroom), (key) =>
      request(app)
        .put(`/api/classrooms/${_id}`)
        .set(headers)
        .send(_.omit(payload.classroom, key))
        .expect(400)
    );
  });

  it("5-3| EXPECT SUCCESS : update classroom with correct payload.classroom ", async () => {
    const db = await dbProvider.get();
    const classroom = await db.collection(COLLECTION.Classroom).findOne();

    const { body } = await request(app)
      .put(`/api/classrooms/${classroom._id}`)
      .set(headers)
      .send(payload.classroom)
      .expect(200);

    const updated = await db
      .collection(COLLECTION.Classroom)
      .findOne({ _id: ObjectId(classroom._id) });

    expect(body).toEqual({
      ...payload.classroom,
      _id: classroom._id.toString(),
    });
    expect(body).toEqual({ ...updated, _id: classroom._id.toString() });
  });

  //DELETE

  it("6-1| EXPECT FAILURE : delete classroom with invalid id ", async () => {
    return await request(app).delete("/api/classrooms/:some_id").expect(400);
  });

  it("6-2| EXPECT SUCCESS : delete classroom ", async () => {
    const db = await dbProvider.get();
    const initCount = _.size(payload.classrooms);
    const classroom = await db.collection(COLLECTION.Classroom).findOne();

    await request(app).delete(`/api/classrooms/${classroom._id}`).expect(204);

    const deletedUser = await db
      .collection(COLLECTION.Classroom)
      .countDocuments({ _id: ObjectId(classroom._id) });

    const afterCount = await db
      .collection(COLLECTION.Classroom)
      .countDocuments();

    expect(afterCount).toEqual(initCount - 1);
    expect(deletedUser).toEqual(0);
  });

  it("6-3| EXPECT SUCCESS : delete classroom  check no users exists ", async () => {
    const [classroom, __] = await initClassroomUser();

    await request(app).delete(`/api/classrooms/${classroom._id}`).expect(204);

    const db = await dbProvider.get();
    const classrommUsersCount = await db
      .collection(COLLECTION.User)
      .countDocuments({ classroom: ObjectId(classroom._id) });

    expect(classrommUsersCount).toEqual(0);
  });
});
