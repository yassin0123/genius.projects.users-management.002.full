module.exports = {
  DB:  "ClassroomAppDB",
  DB_URL_TEMPLATE: 'mongodb://{{dbHost}}:27017/{{db}}',
};