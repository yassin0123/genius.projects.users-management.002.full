const _ = require("lodash");
const { ObjectId } = require("mongodb");
const Promise = require("bluebird");
const request = require("supertest");
const random = require("randomstring");
const payload = require("./payload.json");
const { app, server } = require("../src/app");

const dbProvider = require("./services/db");

const headers = {
  Accept: "application/json",
};

const COLLECTION = {
  Classroom: "Classroom",
  User: "User",
};

const initClassroomUser = async () => {
  const classroom = _.chain(payload.classrooms)
    .head()
    .clone()
    .set("_id", ObjectId())
    .value();
  const user = _.chain(payload.users)
    .head()
    .clone()
    .set("_id", ObjectId())
    .set("classroom", classroom._id)
    .value();

  const db = await dbProvider.get();
  await db.collection(COLLECTION.Classroom).insertOne(classroom);
  await db.collection(COLLECTION.User).insertOne(user);

  return [classroom, user];
};

const initClassroom = async () => {
  const classroom = _.chain(payload.classrooms)
    .head()
    .clone()
    .set("_id", ObjectId())
    .value();

  const user = _.chain(payload.users).head().clone().value();

  const db = await dbProvider.get();
  await db.collection(COLLECTION.Classroom).insertOne(classroom);

  return [classroom, user];
};

describe("SUCCESS", () => {
  beforeEach(async () => {
    const db = await dbProvider.get();
    db.dropDatabase();
  });

  afterEach(async () => {
    const db = await dbProvider.get();
    await db.dropDatabase();
    await dbProvider.close();
    return server.close();
  });

  // LIST

  it("7-1| EXPECT SUCCESS : /classrooms/<id>/users return users list ", async () => {
    const db = await dbProvider.get();

    const classrooms = _.cloneDeep(payload.classrooms);
    await db.collection(COLLECTION.Classroom).insertMany(classrooms);
    const _id = _.head(classrooms)._id.toString();

    const [classroomUsers, others] = _.chain(payload.users)
      .cloneDeep()
      .chunk(2)
      .map((it, index) =>
        index === 0
          ? _.map(it, (el) => _.set(el, "classroom", ObjectId(_id)))
          : it
      )
      .value();

    await db
      .collection(COLLECTION.User)
      .insertMany([...classroomUsers, ...others]);

    const { body } = await request(app)
      .get(`/api/classrooms/${_id}/users`)
      .set(headers)
      .expect(200);

    expect(_.chain(body).value().sort()).toEqual(
      _.map(classroomUsers, (it) => ({
        ...it,
        _id: it._id.toString(),
        classroom: it.classroom.toString(),
      })).sort()
    );
  });

  //GET

  it("8-1| EXPECT FAILURE : fetch user with wrong id ", async () => {
    return await request(app)
      .get("/api/classrooms/:some_id")
      .set(headers)
      .expect(400);
  });

  it("8-2| EXPECT SUCCESS : fetch user by id ", async () => {
    const [classroom, user] = await initClassroomUser();

    const { body } = await request(app)
      .get(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .expect(200);

    expect(body).toEqual({
      ...user,
      _id: user._id.toString(),
      classroom: classroom._id.toString(),
    });
  });

  // CREATE

  it("9-1| EXPECT FAILURE : add user with invalid payload ", async () => {
    const [classroom, user] = await initClassroom();

    const invalid = {
      some_key: "wrong",
    };

    await request(app)
      .post(`/api/classrooms/${classroom._id}/users`)
      .set(headers)
      .send(invalid)
      .expect(400);

    await Promise.map(_.keys(user), (key) =>
      request(app)
        .post(`/api/classrooms/${classroom._id}/users`)
        .set(headers)
        .send(_.omit(user, key))
        .expect(400)
    );
  });

  it("9-2| EXPECT FAILURE : add user with duplicate username ", async () => {
    const [classroom, user] = await initClassroomUser();

    await request(app)
      .post(`/api/classrooms/${classroom._id}/users`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: user.username,
        email: "john_random@example1.com",
        age: 24,
        role: "STUDENT",
      })
      .expect(400);
  });

  it("9-3| EXPECT FAILURE : add user with duplicate email ", async () => {
    const [classroom, user] = await initClassroomUser();

    await request(app)
      .post(`/api/classrooms/${classroom._id}/users`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: "random",
        email: user.email,
        age: 24,
        role: "STUDENT",
      })
      .expect(400);
  });

  it("9-4| EXPECT FAILURE : add duplicate teacher ", async () => {
    const [classroom, user] = await initClassroomUser();

    const existingUser = {
      firstName: "john",
      classroom: ObjectId(classroom._id),
      lastName: "doe",
      username: "johndoe2",
      email: "john@example2.com",
      age: 24,
      role: "TEACHER",
    };

    const db = await dbProvider.get();
    await db.collection(COLLECTION.User).insertOne(existingUser);

    

    await request(app)
      .post(`/api/classrooms/${classroom._id}/users`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: "some_other_username",
        email: "john_random@example1.com",
        age: 24,
        role: "TEACHER",
      })
      .expect(400);
  });

  it("9-5| EXPECT SUCCESS : add user with correct payload ", async () => {
    const [classroom, user] = await initClassroom();

    const initCount = 0;

    const { body } = await request(app)
      .post(`/api/classrooms/${classroom._id}/users`)
      .set(headers)
      .send(user)
      .expect(201);

    const db = await dbProvider.get();
    const afterCount = await db.collection(COLLECTION.User).countDocuments();
    const dbUser = await db
      .collection(COLLECTION.User)
      .findOne({ _id: ObjectId(body._id) });

    expect(_.omit(body, "_id")).toEqual(user);
    expect(afterCount).toEqual(initCount + 1);
    expect(body).toEqual({ ...dbUser, _id: dbUser._id.toString() });
  });

  // UPDATE

  it("10-1| EXPECT FAILURE : update user with invalid id ", async () => {
    const [classroom, user] = await initClassroom();

    return await request(app)
      .put(`/api/classrooms/${classroom._id}/users/wronggg_id`)
      .send(user)
      .set(headers)
      .expect(400);
  });

  it("10-2| EXPECT FAILURE : update user with invalid payload ", async () => {
    const [classroom, user] = await initClassroomUser();

    const invalid = {
      some_key: "wrong",
    };

    await request(app)
      .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .send(invalid)
      .expect(400);

    await Promise.map(_.keys(user), (key) =>
      request(app)
        .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
        .set(headers)
        .send(_.omit(user, key))
        .expect(400)
    );
  });

  it("10-3| EXPECT FAILURE : update user with duplicate username ", async () => {
    const existingUser = {
      firstName: "john",
      lastName: "doe",
      username: "johndoe2",
      email: "john@example2.com",
      age: 24,
      role: "STUDENT",
    };

    const db = await dbProvider.get();
    await db.collection(COLLECTION.User).insertOne(existingUser);

    const [classroom, user] = await initClassroomUser();

    await request(app)
      .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: existingUser.username,
        email: "john_random@example1.com",
        age: 24,
        role: "STUDENT",
      })
      .expect(400);
  });

  it("10-4| EXPECT FAILURE : update user with duplicate email ", async () => {
    const existingUser = {
      firstName: "john",
      lastName: "doe",
      username: "johndoe2",
      email: "john@example2.com",
      age: 24,
      role: "STUDENT",
    };

    const db = await dbProvider.get();
    await db.collection(COLLECTION.User).insertOne(existingUser);

    const [classroom, user] = await initClassroomUser();

    await request(app)
      .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: "random",
        email: existingUser.email,
        age: 24,
        role: "STUDENT",
      })
      .expect(400);
  });

  it("10-5| EXPECT FAILURE : update user role to a duplicate teacher ", async () => {
    const [classroom, user] = await initClassroomUser();

    const existingUser = {
      firstName: "john",
      lastName: "doe",
      username: "johndoe2",
      classroom: ObjectId(classroom._id),
      email: "john@example2.com",
      age: 24,
      role: "TEACHER",
    };

    const db = await dbProvider.get();
    await db.collection(COLLECTION.User).insertOne(existingUser);

    await request(app)
      .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .send({
        firstName: "first",
        lastName: "last",
        username: 'some_other_username',
        email: "john_random@example1.com",
        age: 24,
        role: "TEACHER",
      })
      .expect(400);
  });

  it("10-6| EXPECT SUCCESS : update user with correct payload ", async () => {
    const [classroom, user] = await initClassroomUser();
    const update = {
      ..._.omit(user, ["_id", "classroom"]),
      firstName: "new",
      lastName: "new",
    };

    const { body } = await request(app)
      .put(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .set(headers)
      .send(update)
      .expect(200);

    const db = await dbProvider.get();
    const updated = await db
      .collection(COLLECTION.User)
      .findOne({ _id: ObjectId(user._id) });

    expect(body).toEqual({
      ...update,
      _id: user._id.toString(),
      classroom: classroom._id.toString(),
    });
    expect(body).toEqual({
      ...updated,
      _id: user._id.toString(),
      classroom: classroom._id.toString(),
    });
  });

  //DELETE

  it("11-1| EXPECT FAILURE : delete user with invalid id ", async () => {
    const [classroom, __] = await initClassroom();
    return await request(app)
      .delete(`/api/classrooms/${classroom._id}/users/some_id`)
      .expect(400);
  });

  it("11-2| EXPECT SUCCESS : delete user ", async () => {
    const [classroom, user] = await initClassroomUser();

    const initCount = 1;

    await request(app)
      .delete(`/api/classrooms/${classroom._id}/users/${user._id}`)
      .expect(204);

    const db = await dbProvider.get();
    const deletedUser = await db
      .collection(COLLECTION.User)
      .countDocuments({ _id: ObjectId(user._id) });

    const afterCount = await db.collection(COLLECTION.User).countDocuments();

    expect(afterCount).toEqual(initCount - 1);
    expect(deletedUser).toEqual(0);
  });
});
