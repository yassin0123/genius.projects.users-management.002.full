var express = require("express");

const validate = require("./validate");
const controller = require("./controller");

module.exports = express
  .Router()
  .use("/:_id/users", controller.propagateParams, require("./users"))
  .get("/", controller.list)
  .get("/:_id", validate.exists, controller.item)
  .post("/", validate.post, controller.add)
  .put("/:_id", validate.put, controller.update)
  .delete("/:_id", validate.exists, controller.delete);
