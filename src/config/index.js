module.exports = {
  LOG_LEVEL:  "info",
  HOST:  "0.0.0.0",
  PORT:  8080,
  AUTH_SECRET: "thisisverrysecret",
  DB: process.env.DB || "ClassroomAppDB",
  DB_URL_TEMPLATE: 'mongodb://{{dbHost}}:27017/{{db}}',
  AUTH_SECRET: "secretauthkey"
};